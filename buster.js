var _ = require('lodash');
var Promise = require('bluebird');
var request = require('request-promise');
var cheerio = require('cheerio');
var format = require('date-format');
var minimist = require('minimist');
var argv = minimist(process.argv.slice(2));
var cron = require('node-cron');
var fs = require('fs')
var config=require("./config");
var favourites=require("./favourites");
var stringArgv = require('string-argv');
var cookieJar = request.jar();
var headers = {
	'User-Agent': 
		'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'+
		' Ubuntu Chromium/53.0.2785.143 Chrome/53.0.2785.143 Safari/537.36'
};

if (argv.file){
	processFile(argv.file);
} else {
	Buster(argv)
}

function processFile(fileName){
	return fs.readFile(fileName, 'utf8', function(err, data) {
		if (err) 
			throw err;
		var job=Promise.resolve(null);
		return data.match(/[^\r\n]+/g)
		.forEach(function(line){
			if (line.match(/^#|^\s$/))
				return;
			job=job.then(function(){
				return Buster(
					minimist(stringArgv(line))
				)
			})
		})
		return job;
	})
}

function inThePast(argv){
var today = new Date()	
	dformat=today.today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate()
	if (argv.date < dformat)
		return true;
	if (argv.date > dformat)
		return false;
	tformat=today.getHours() + ":" + today.getMinutes()
	if (argv.time < tformat)
		return true;
}

function Buster(argv){
	if (inThePast(argv))
		return;

console.log("ARGV="+JSON.stringify(argv));
	if (!argv.date){
		console.log("NO DATE GIVEN, NEED -date=<date>");
		process.exit(-1);
	}

	var DEFAULT_TIME="13:00";
	var DEFAULT_DURATION=90;
	var DEFAULT_WAIT=0;//120;
	var DEFAULT_RANDOM_WAIT=0;//30;
	var DEFAULT_RESCHEDULE=15;

	var target=argv;
	target.starttime=argv.starttime||argv.time||DEFAULT_TIME
	target.start=Date.parse(argv.date+" "+target.starttime)/(60*1000)
	target.duration=argv.duration||DEFAULT_DURATION
	target.session_left=argv.session_left||DEFAULT_DURATION
	target.session_duration=argv.session_duration||DEFAULT_DURATION
	target.finish=target.start+target.duration
	target.endtime= format("yy/MM/dd hh:mm",new Date(target.finish*60*1000))
	target.session={
		start:target.start,
		finish:(target.start*1)+target.session_left
	}



	da=/(\d+)\-(\d+)\-(\d+)/g.exec(argv.date)
	da.shift()
	target.dato=da.join('')
	target.slash_date=da.join('/')

	if (target.user_name === undefined)
		target.user_name = config.USER_NAME
	if (target.user_id === undefined)
		target.user_id = config.USER_ID
	if (target.user_password === undefined)
		target.user_password = config.USER_PASSWORD
	
	if (target.wait === undefined)
		target.wait = DEFAULT_WAIT
	if (target.random_wait === undefined)
		target.random_wait = DEFAULT_RANDOM_WAIT
	if (target.reschedule === undefined)
		target.reschedule= DEFAULT_RESCHEDULE

	var runTime=(target.finish-60*24*3) * 60*1000

	if (runTime < Date.now())
		return BloodVessel(target);

	date = new Date(
		runTime + (Math.random() * target.random_wait + target.wait)*1000*60
	)

	cronString=[
		date.getSeconds(),
		date.getMinutes(),
		date.getHours(),
		date.getDate(),
		date.getMonth()+1,
		date.getDay()
	].join(' ')

console.log("Scheduling @ "+format("yy/MM/dd hh:mm",new Date(date))+"\nCRON="+cronString+"\nJob="+JSON.stringify(target));
	return cron.schedule(cronString,
		function(){
			return BloodVessel(target)
		}
	)
}

function BloodVessel(target){
	var form ={};
	form['authenticate-useraccount']= target.user_id;
	form['authenticate-password']= target.user_password;
	form['authenticate-url']= '/public/';
	form['authenticate-verification']= 'ok';
	var rows={};
	var clear_rooms={};
	 
	return request.post('https://rncm.asimut.net/public/login.php', {
		form:form,
		jar: cookieJar,
		headers:headers
	})
	.catch(function(){
		return;
	})
	.then(function(){
		return request({
			url:'https://rncm.asimut.net/public/index.php?dato='+target.dato+'&akt=visgruppe&id=12',
			method: 'GET', 
			headers:headers,
			jar:cookieJar,
		})
	})
	.then(function(body){
		var $ = cheerio.load(body)
		$('.chart-row').each(function(){
			var row=rowNumber($(this).attr('id'))
			$(this).find('.event-location')
			.each(function(){
				rows[row]={
					location:$(this).attr('title'),
					location_id:$(this).attr('rel')
				}
			})
		})
		return request({
			url:"https://rncm.asimut.net/public/async_fetchevents.php?starttime="+target.date+"&endtime="+target.date+"&locationgroup=-12", 
			method: 'GET', 
			headers:headers,
			jar:cookieJar,
			json:true
		})
	})
	.then(function(json){
		clear_rooms=_.clone(rows);

		var already=undefined;
		_.each(json,function(row){
			var booking={};
			'event_id start duration row class name'
			.split(' ')
			.forEach(function(field){
				booking[field]=row.shift();
			});
			if (!rows[booking.row])
				return;
			booking.start=booking.start*1;
			booking.duration=booking.duration*1;
			booking.finish=booking.duration+booking.start;
			booking.starttime= format("hh:mm",new Date(booking.start*60*1000))
			booking.endtime= format("hh:mm",new Date(booking.finish*60*1000))

			if (target.start===booking.finish && booking.name === target.user_name)
				already=booking;
			else if (target.start-booking.finish <= 60 && booking.name === target.user_name)
				delete clear_rooms[booking.row]

			if (overlap(target,booking)){
				if (booking.name === target.user_name){
					console.log("You have an overlapping or identical booking "+target.date+" "+target.starttime)
					throw new Error("IDENTICAL OR OVERLAPPING BOOKING, IGNORING");
//					return reschedule_booking(target)
				}
				delete rows[booking.row]
			}

			if (overlap(target.session,booking)){
				if (booking.name === target.user_name)
					already=booking;
				else
					delete clear_rooms[booking.row]
			}
		})

		if (!_.keys(rows).length){
			console.log("NO ROOMS AVAILABLE FOR "+JSON.stringify(target,null,2))
			return reschedule_booking(target)
		}

console.log(_.keys(rows).length+" ROOMS AVAILABLE");

		if (already){
console.log("ALREADY IN ="+already.row);
			if (rows[already.row]){
				rows[already.row].already=already;
console.log("STILL FREE SO CONTINUING");
				return rows[already.row];
			}
console.log("GOT TO MOVE");
		}

		var choice=undefined;
		var not_booked=_.keys(clear_rooms).length ? clear_rooms : rows;
		_.each(randomise(favourites),function(fav){
			_.each(not_booked,function(room,row){
				if (!choice && room.location === fav)
					choice=row;
			});
		})
		if (!choice)
			choice=_.keys(not_booked)[0]

		return rows[choice]
	})
	.then(function(row){
		if (!row || row < 0)
			return;
		if (row.already)
			return extend_booking(row.already,row,target)
		return create_booking(row,target)
	})
	.then(function(response){
		if (response && response[0]){
			if (response[0].text.match(/too close/))
				return reschedule_booking(target)
			console.log(response[0].text)
		}
	})
	.catch(function(err){
		if (err && err.message && err.message.match(/too close/))
			return reschedule_booking(target)
			
console.log("ERROR, CANT BOOK ROOM "+err);
	});
}

function	reschedule_booking(target){
	if (!target.reschedule)
		return;
	target.start=target.start+target.reschedule
	target.starttime=format("hh:mm",new Date(target.start*60*1000))

	if (target.starttime.match(/^23/))
		throw new Error("THE ENTIRE DAY IS FULL, THIS HAS TO BE A PROGRAM ERROR "+JSON.stringify(target,null,2))
		
	console.log("RESCHEDULING TO "+target.starttime);
	return Buster(target)
}


function create_booking(row,target){
	var form={};
	form['event-id']=0
	form['location-id']=row.location_id
	form.category=35
	form.date=target.slash_date
	form.starttime=target.starttime
	form.endtime=target.endtime
	form.location=row.location
	form.description="practice"
	form.action='save'
console.log("CREATE BOOKING "+JSON.stringify(form,null,2));
	return request.post('https://rncm.asimut.net/public/async-event.php', {
		form:form,
		jar: cookieJar,
		headers:headers,
		json:true
	})
}

function extend_booking(booking,row,target){
	var form={};
	console.log("EXTEND TO "+target.endtime+" FROM "+booking.endtime)
	if (target.finish < booking.finish)
		return;
	form['event-id']=booking.event_id
	form['location-id']=row.location_id
	form.category=35
	form.date=target.slash_date

	if (target.finish - booking.start > target.session_duration){
		booking.start=target.finish - target.session_duration;
		booking.starttime= format("hh:mm",new Date(booking.start*60*1000))
	}

	form.starttime=booking.starttime
	form.endtime=target.endtime
	form.location=row.location
	form.description="practice"
	form.action='save'
console.log("UPDATE BOOKING "+JSON.stringify(form,null,2));
	return request.post('https://rncm.asimut.net/public/async-event.php', {
		form:form,
		jar: cookieJar,
		headers:headers,
		json:true
	})
}

function overlap(a,b){
	return (a.start < b.finish) && (a.finish > b.start)
}

function rowNumber(rowId) {
	return /chart\-row\-(\d+)/g.exec(rowId)[1];
}

function randomise(list,range){
	range=range||list.length;
	var array=_.clone(list)
	_.each(array,function(value,index){
		var swapi= 
						Math.floor(
							Math.random() * Math.min(range,array.length-index)
						)
						+index
		array[index]=array[swapi];
		array[swapi]=value;
	})
	return array;
}

