var sprintf=require("sprintf").sprintf
var _ = require('lodash');
var minimist = require('minimist');
var argv = minimist(process.argv.slice(2));
var cron = require('node-cron');
var fs = require('fs')
var stringArgv = require('string-argv');
var holidays = require('./holidays.js');

duplicate(process.argv[2],process.argv[3])

function duplicate(fileName,weeks){
	weeks=parseInt(weeks)
	var n=0
	return fs.readFile(fileName, 'utf8', function(err, data) {
		while (n++ < weeks){
			data.match(/[^\r\n]+/g)
			.forEach(function(line){
				if (line.match(/^#|^\s$/)){
					console.log(line)
					return;
				}
				console.log(newbooking(n*7,line))
			})
		}		
	})
}

function newbooking(days,line){
	var argv=minimist(stringArgv(line))
	argv.date=addDays(argv.date,days)
	if (holidays[argv.date])
		return "#"+argv.date+" IS A HOLIDAY"
	var s=""
	_.each(argv,function(val,name){
		if (!name || !val || name=='_')
			return
		s+="--"+name+"="+val+" "
	})
	return s
}

function addDays(date, days) {
  date.replace('-','/')
  var result = new Date(date);
 d=parseInt(days)
 result.setDate(result.getDate() + d);
  return sprintf("%02d-%02d-%02d",result.getFullYear(),(result.getMonth()+1),result.getDate())
}
